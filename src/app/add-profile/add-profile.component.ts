import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.component.html',
  styleUrls: ['./add-profile.component.css']
})
export class AddProfileComponent {
  @Input() isVisible: boolean = false;
  @Input() showAddProfile: () => void = () => {return };
  userId: string = "";

  form: FormGroup = new FormGroup({
    name: new FormControl("",[
        Validators.required,
    ]),
    surname: new FormControl("",[
        Validators.required,
    ]),
    sexe: new FormControl("",[
        Validators.required,
    ]),
    birthdate: new FormControl("",[
        Validators.required,
    ]),
    ranking: new FormControl(""),
    level: new FormControl("",[
        Validators.required,
    ]),
    lessontype: new FormControl("",[
      Validators.required,
  ])
  });
  loading = false;
  submitted = false;

  get name() { return this.form.get('name'); }
  get surname() { return this.form.get('surname'); }
  get sexe() { return this.form.get('sexe'); }
  get birthdate() { return this.form.get('birthdate'); }
  get ranking() { return this.form.get('ranking'); }
  get level() { return this.form.get('level'); }
  get lessontype() { return this.form.get('lessontype'); }

  //Function launch when component created
  ngOnInit(): void {
    //Find User Lessons data !
    let session = sessionStorage.getItem('userData')
    let userData = session ? JSON.parse(session) : undefined;
    this.userId = userData.id;
  }

  onSubmit() {
      this.submitted = true;

      //Stop submit if invalid
      if (this.form.invalid) {
          return;
      }

      this.loading = true;

      //API Call to create profile
      API.post('/profil',{
        userId: this.userId,
        nom: this.form.get('name')?.value,
        prenom: this.form.get('surname')?.value,
        sexe: this.form.get('sexe')?.value,
        dateNaissance: this.form.get('birthdate')?.value,
        classement: this.form.get('ranking')?.value,
        niveau: this.form.get('level')?.value,
        typeCours: this.form.get('lessontype')?.value,
      })
      .then((res) => {
          //Pass if success
          console.log('Success', res)
          location.reload();
          this.showAddProfile();
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
          this.loading = false;
      })
  }
}

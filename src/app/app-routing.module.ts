import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { StudentsComponent } from './students/students.component';
import { TeachersComponent } from './teachers/teachers.component';
import { LessonsComponent } from './lessons/lessons.component';
import { LessonRequestsComponent } from './lesson-requests/lesson-requests.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'profiles', component: ProfilesComponent},
  { path: 'teachers', component: TeachersComponent},
  { path: 'students', component: StudentsComponent},
  { path: 'lessons', component: LessonsComponent},
  { path: 'request', component: LessonRequestsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

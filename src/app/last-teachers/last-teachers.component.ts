import { Component } from '@angular/core';
import API from '../api';

interface Teacher {
  id: string,
  user_name: string,
  user_surname: string,
  ranking: string,
}

@Component({
  selector: 'app-last-teachers',
  templateUrl: './last-teachers.component.html',
  styleUrls: ['./last-teachers.component.css']
})
export class LastTeachersComponent {
  teachers: Array<Teacher> = [];

  addTeacherVisible: boolean = false;

  //Function launch when component created
  ngOnInit(): void {
      //Find User Teachers data !
      let session = sessionStorage.getItem('userData')
      let userData = session ? JSON.parse(session) : undefined;
      if(userData) this.findTeachers(userData.id); 
  }

  //Call API for Teachers
  findTeachers(id: number): void{
    //Init new Array
    let newProfiles: Teacher[] = []

    API.get('/profil')
    .then((res) => {
      res.data.filter((element: any) => element.professeur).forEach((element: any) => {
        //Create new Teacher
        let newTeacher:Teacher = {
          id: element.id,
          user_name: element.nom,
          user_surname: element.prenom,
          ranking: element.classement,
        }

        //Add new Teacher
        newProfiles.push(newTeacher)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')

        //Edit component Teachers
        this.teachers = newProfiles
    })
  }

  //Show add teacher menu
  showAddTeacher = ():  void => {
    this.addTeacherVisible=!this.addTeacherVisible
  }
}

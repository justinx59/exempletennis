import axios from 'axios';

//Communication avec l'API
const instance = axios.create({
  baseURL: `https://localhost:44379/api/`,
  withCredentials: true,
  headers: {
    'X-Requested-With' : 'XMLHttpRequest',
    "Access-Control-Allow-Origin": "*",
    'Accept' : 'application/json',
    'Content-Type' : 'application/json',
    'Authorization': 'Bearer ' + sessionStorage.getItem('token'),
  }
});

export default instance;
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: [ './hello.component.css' ]
})

export class HelloComponent{
    name: string = "guest";
    surname: string = "";

    //Function launch when component created
    ngOnInit(): void {
        //Find User data !
        let session = sessionStorage.getItem('userData')
        let userData = session ? JSON.parse(session) : undefined;
        this.name = userData?userData.nom:"guest"; 
        this.surname = userData?userData.prenom:""; 
  }
}
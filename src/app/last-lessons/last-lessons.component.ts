import { Component } from '@angular/core';
import API from '../api';

interface Lesson {
  id: string,
}

@Component({
  selector: 'app-last-lessons',
  templateUrl: './last-lessons.component.html',
  styleUrls: ['./last-lessons.component.css']
})
export class LastLessonsComponent {
  lessons: Array<Lesson> = [];

  //Function launch when component created
  ngOnInit(): void {
      //Find User Last Profiles data !
      let session = sessionStorage.getItem('userData')
      let userData = session ? JSON.parse(session) : undefined;
      if(userData) this.findLessons(userData.id); 
  }

  //Call API for Last Lesson
  findLessons(id: number): void{
    //Init new Array
    let newLessons: Lesson[] = []

    API.get('/demandecours')
    .then((res) => {
      res.data.forEach((element: any) => {
        //Create new Last Lesson
        let newLesson:Lesson = {
          id: element.id,
        }

        //Add new Last Lesson
        newLessons.push(newLesson)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')

        //Edit component Last Lessons
        this.lessons = newLessons
    })
  }
}

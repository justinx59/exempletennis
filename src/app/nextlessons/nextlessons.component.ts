import { Component, OnInit } from '@angular/core';

interface Lesson {
  date: Date,
  user_name: string,
  user_surname: string,
  total_users: number,
  start_hour: Date,
  end_hour: Date,
}

@Component({
  selector: 'app-nextlessons',
  templateUrl: './nextlessons.component.html',
  styleUrls: [ './nextlessons.component.css' ]
})

export class NextLessonsComponent{
    lessons: Array<Lesson> = [];

    //Function launch when component created
    ngOnInit(): void {
        //Find User Lessons data !
        let session = sessionStorage.getItem('userData')
        let userData = session ? JSON.parse(session) : undefined;
        if(userData) this.findLessons(userData.id); 
    }

    //Call API for lessons
    findLessons(id: number): void{
      //Init new Array
      let newLessons = []

      //Create new Lesson
      let newLesson:Lesson = {
        date: new Date(),
        user_name: "Who",
        user_surname: "Donna",
        total_users: 14,
        start_hour: new Date(),
        end_hour: new Date(),
      } 

      //Add new Lesson
      newLessons.push(newLesson)

      //Edit component Lessons
      this.lessons = newLessons
    }
}
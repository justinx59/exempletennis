import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
    userConnected: boolean = false;
    isResponsable: boolean = false;
    isTeacher: boolean = false;

    //Function launch when component created
    ngOnInit(): void {
        //Find User data !
        let session = sessionStorage.getItem('userData')
        let userData = session ? JSON.parse(session) : undefined;
        this.userConnected = userData?userData.id:false;  
        this.isResponsable = userData?userData.responsable:false; 
        this.isTeacher = userData?userData.profils.find((elmt: any) => elmt.professeur===true)!==undefined:false;
    }

    //Disconnect User
    logout(): void {
      sessionStorage.removeItem('userData');
      sessionStorage.removeItem('token');
      location.reload();
    }
}

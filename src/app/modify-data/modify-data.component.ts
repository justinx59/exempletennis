import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

interface Profile{
  id: string,
  name: string,
  surname: string,
  sexe: string,
  lessonType: string,
  birthDate: Date,
  blessed: boolean,
  level: string,
  ranking: string,
  comment: string,
}

@Component({
  selector: 'app-modify-data',
  templateUrl: './modify-data.component.html',
  styleUrls: ['./modify-data.component.css']
})

export class ModifyDataComponent {
  @Input() isVisible: boolean = false;
  @Input() showModifyData: () => void = () => {return };
  @Input() index: number = 0;
  @Input() profiles: Array<Profile> = []

  form: FormGroup = new FormGroup({});
  loading = false;
  submitted = false;

  ngOnChanges(): void{
    this.form = new FormGroup({
      name: new FormControl(this.profiles[this.index].name,[
          Validators.required,
      ]),
      surname: new FormControl(this.profiles[this.index].surname,[
          Validators.required,
      ]),
      sexe: new FormControl(this.profiles[this.index].sexe,[
          Validators.required,
      ]),
      birthdate: new FormControl(this.profiles[this.index].birthDate.toISOString().split('T')[0],[
          Validators.required,
      ]),
      ranking: new FormControl(this.profiles[this.index].ranking),
      level: new FormControl(this.profiles[this.index].level,[
          Validators.required,
      ]),
      lessontype: new FormControl(this.profiles[this.index].lessonType,[
        Validators.required,
      ])
    })
  }

  get name() { return this.form.get('name'); }
  get surname() { return this.form.get('surname'); }
  get sexe() { return this.form.get('sexe'); }
  get birthdate() { return this.form.get('birthdate'); }
  get ranking() { return this.form.get('ranking'); }
  get level() { return this.form.get('level'); }
  get blessed() { return this.form.get('blessed'); }
  get lessontype() { return this.form.get('lessontype'); }

  onSubmit() {
      this.submitted = true;

      //Stop submit if invalid
      if (this.form.invalid) {
          return;
      }

      this.loading = true;

      //API Call to create profile
      API.put(`/profil/${this.profiles[this.index].id}`,{
        id: this.profiles[this.index].id,
        nom: this.form.get('name')?.value,
        prenom: this.form.get('surname')?.value,
        sexe: this.form.get('sexe')?.value,
        dateNaissance: this.form.get('birthdate')?.value,
        classement: this.form.get('ranking')?.value,
        niveau: this.form.get('level')?.value,
        typeCours: this.form.get('lessontype')?.value,
        blesse: this.form.get('blessed')?.value,
      })
      .then((res) => {
          //Pass if success
          console.log('Success', res)
          location.reload();
          this.showModifyData();
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
          this.loading = false;
      })
  }
}

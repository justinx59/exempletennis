import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})

export class LoginComponent{
    form: FormGroup = new FormGroup({
        username: new FormControl("",[
            Validators.required,
        ]),
        password: new FormControl("",[
            Validators.required,
        ]),
        stayconnected: new FormControl(false)
    });
    loading = false;
    submitted = false;
    passwordError = false;

    get username() { return this.form.get('username'); }
    get password() { return this.form.get('password'); }
    get stayconnected() { return this.form.get('stayconnected')}

    onSubmit() {
        this.submitted = true;
        this.passwordError = false

        //Stop submit if invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;

        //API Call to login
        API.post('/user/login',{
            email: this.form.get('username')?.value,
            password: this.form.get('password')?.value,
        })
        .then((res) => {
            //Pass if success
            console.log('Success', res.data)
            sessionStorage.setItem('token', res.data);

            API.get('/user/getiduserlogged',{
                headers: {
                    'Authorization': 'Bearer ' + res.data
                }
            })
            .then((res) => {
                API.get(`/user/${res.data}`,{
                    headers: {
                        'Authorization': 'Bearer ' + sessionStorage.getItem('token')
                    }
                })
                .then((res) => {
                    sessionStorage.setItem('userData', JSON.stringify(res.data));
                    location.reload();
                })
                .catch((err) => {
                    //Pass if errors
                    console.log('Error', err)
                })
                .finally(() => {
                    //Pass all time
                    console.log('End')
                    this.loading = false;
                })
            })
            .catch((err) => {
                //Pass if errors
                console.log('Error', err)
            })
            .finally(() => {
                //Pass all time
                console.log('End')
                this.loading = false;
            })
        })
        .catch((err) => {
            //Pass if errors
            console.log('Error', err)
            
            if(err.response.data==="Wrong password")
                this.passwordError = true;
        })
        .finally(() => {
            //Pass all time
            console.log('End')
            this.loading = false;
        })
    }
}
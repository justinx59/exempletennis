import { Component } from '@angular/core';
import API from '../api';

interface Student{
  id: string,
  name: string,
  surname: string,
  sexe: string,
  lessonType: string,
  birthDate: Date,
  blessed: boolean,
  level: string,
  ranking: string,
  comment: string,
}

interface Lesson{
  id: string,
}

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})

export class StudentsComponent {
  students: Array<Student> = []
  lessons: Array<Lesson> = []
  indexSelected: number = 0;

  //Function launch when component created
  ngOnInit(): void {
      //Find User Lessons data !
      let session = sessionStorage.getItem('userData')
      let userData = session ? JSON.parse(session) : undefined;
      if(userData) this.findProfiles(userData.id); 
  }

  //Call API for profiles
  findProfiles(id: string): void{
    //Init new Array
    let newStudents: Student[] = []

    let session = sessionStorage.getItem('userData');
    let userData = session ? JSON.parse(session) : undefined;

    if(userData){
      API.get(`/profil`)
      .then((res) => {
        res.data.filter((element: any) => !element.professeur).forEach((element: any) => {
          let newTeacher: Student = {
            id: element.id,
            name: element.nom,
            surname: element.prenom,
            sexe: element.sexe,
            lessonType: element.typeCours,
            birthDate: new Date(element.dateNaissance),
            blessed: element.blesse,
            level: element.niveau,
            ranking: element.classement,
            comment: element.commentaire,
          }

          //Add new Student
          newStudents.push(newTeacher)
        });
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
          //Edit component Students
          this.students = newStudents
      })
    };
  }

  //Change selected profile
  selectProfile(index: number): void{
    this.indexSelected=index;
  }
}

import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import {MatIconModule} from '@angular/material/icon'
import { adapterFactory } from 'angular-calendar/date-adapters/moment';
import * as moment from 'moment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { RegisterComponent } from './register/register.component';
import { HelloComponent } from './hello/hello.component';
import { NextLessonsComponent } from './nextlessons/nextlessons.component';
import { AddProfileComponent } from './add-profile/add-profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LastProfilesComponent } from './last-profiles/last-profiles.component';
import { LastTeachersComponent } from './last-teachers/last-teachers.component';
import { AddTeacherComponent } from './add-teacher/add-teacher.component';
import { LastLessonsComponent } from './last-lessons/last-lessons.component';
import { TeachersComponent } from './teachers/teachers.component';
import { StudentsComponent } from './students/students.component';
import { FooterComponent } from './footer/footer.component';
import { ModifyDataComponent } from './modify-data/modify-data.component';
import { LessonsComponent } from './lessons/lessons.component';
import { RequestLessonComponent } from './request-lesson/request-lesson.component';
import { LessonRequestsComponent } from './lesson-requests/lesson-requests.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { AddLessonComponent } from './add-lesson/add-lesson.component';
import { AddToGroupComponent } from './add-to-group/add-to-group.component';

export function momentAdapterFactory() {
  return adapterFactory(moment);
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    HomeComponent,
    ProfilesComponent,
    RegisterComponent,
    HelloComponent,
    NextLessonsComponent,
    AddProfileComponent,
    LastProfilesComponent,
    LastTeachersComponent,
    AddTeacherComponent,
    LastLessonsComponent,
    TeachersComponent,
    StudentsComponent,
    FooterComponent,
    ModifyDataComponent,
    LessonsComponent,
    RequestLessonComponent,
    LessonRequestsComponent,
    AddGroupComponent,
    AddLessonComponent,
    AddToGroupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: momentAdapterFactory }),
    BrowserAnimationsModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})

export class AddGroupComponent {
    @Input() isVisible: boolean = false;
    @Input() showAddGroup: () => void = () => {return };
    userId: string = "";
  
    form: FormGroup = new FormGroup({
      groupSize: new FormControl("",[
          Validators.required,
      ]),
      cycle: new FormControl("",[
          Validators.required,
      ]),
      day: new FormControl("",[
        Validators.required,
      ]),
      startTime: new FormControl("",[
        Validators.required,
      ]),
      endTime: new FormControl("",[
        Validators.required,
      ]),
    });
    loading = false;
    submitted = false;
  
    get groupSize() { return this.form.get('groupSize'); }
    get cycle() { return this.form.get('cycle'); }
    get day() { return this.form.get('day'); }
    get startTime() { return this.form.get('startTime'); }
    get endTime() { return this.form.get('endTime'); }
  
    //Function launch when component created
    ngOnInit(): void {
      //Find User Lessons data !
      let session = sessionStorage.getItem('userData')
      let userData = session ? JSON.parse(session) : undefined;
      this.userId = userData.id;
    }
  
    onSubmit() {
        this.submitted = true;
  
        //Stop submit if invalid
        if (this.form.invalid) {
            return;
        }
  
        this.loading = true;
  
        //API Call to create profile
        API.post('/groupecours',{
          tailleGroupe: this.form.get('groupSize')?.value,
          heureDebut: this.form.get('startTime')?.value+":00",
          heureFin: this.form.get('endTime')?.value+":00",
          jour: this.form.get('day')?.value,
          periode: this.form.get('cycle')?.value,
        })
        .then((res) => {
            //Pass if success
            console.log('Success', res)
            location.reload();
            this.showAddGroup();
        })
        .catch((err) => {
            //Pass if errors
            console.log('Error', err)
        })
        .finally(() => {
            //Pass all time
            console.log('End')
            this.loading = false;
        })
    }
}

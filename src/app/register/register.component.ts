import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [ './register.component.css' ]
})

export class RegisterComponent{
    form: FormGroup = new FormGroup({
        name: new FormControl("",[
            Validators.required,
        ]),
        surname: new FormControl("",[
            Validators.required,
        ]),
        email: new FormControl("",[
            Validators.required,
        ]),
        phone: new FormControl("",[
            Validators.required,
        ]),
        password: new FormControl("",[
            Validators.required,
        ]),
        confirm_password: new FormControl("",[
            Validators.required,
        ])
    });
    loading = false;
    submitted = false;

    get name() { return this.form.get('name'); }
    get surname() { return this.form.get('surname'); }
    get email() { return this.form.get('email'); }
    get phone() { return this.form.get('phone'); }
    get password() { return this.form.get('password'); }
    get confirm_password() { return this.form.get('confirm_password'); }

    onSubmit() {
        this.submitted = true;

        //Stop submit if invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;

        //API Call to register
        API.post('/user/register',{
            nom: this.form.get('name')?.value,
            prenom: this.form.get('surname')?.value,
            email: this.form.get('email')?.value,
            numTelephone: this.form.get('phone')?.value,
            password: this.form.get('password')?.value
        })
        .then((res) => {
            //Pass if success
            console.log('Success', res)
            location.reload();
        })
        .catch((err) => {
            //Pass if errors
            console.log('Error', err)
        })
        .finally(() => {
            //Pass all time
            console.log('End')
            this.loading = false;
        })
    }
}
import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

interface Groupe{
  id: string,
  day: string,
  startHour: string,
  endHour: string,
  period: string,
  size: number,
  profils: Array<string>
}

@Component({
  selector: 'app-add-lesson',
  templateUrl: './add-lesson.component.html',
  styleUrls: ['./add-lesson.component.css']
})

export class AddLessonComponent {
  @Input() isVisible: boolean = false
  @Input() showAddLesson: () => void = () => {return };
  groups: Array<Groupe> = [];

  form: FormGroup = new FormGroup({
    date: new FormControl("",[
        Validators.required,
    ]),
    group: new FormControl("",[
        Validators.required,
    ]),
  });
  loading = false;
  submitted = false;

  get date() { return this.form.get('date'); }
  get group() { return this.form.get('group'); }

  //Function launch when component created
  ngOnInit(): void {
    this.getGroups();
  }

  getGroups(): void{
    //Init new Array
    let newGroups: Groupe[] = []

    API.get(`/groupecours`)
    .then((res) => {
      res.data.forEach((element: any) => {
        let newGroup: Groupe = {
          id: element.id,
          day: element.jour,
          startHour: element.heureDebut,
          endHour: element.heureFin,
          period: element.periode,
          size: element.tailleGroupe,
          profils: element.profils.map((profil : any) => {return profil.id})
        }

        //Add new Group
        newGroups.push(newGroup)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
        //Edit component Groups
        this.groups = newGroups
    })
  }

  onSubmit() {
      this.submitted = true;

      //Stop submit if invalid
      if (this.form.invalid) {
          return;
      }

      this.loading = true;

      //API Call to create profile
      API.post('/cours',{
        date: this.form.get('date')?.value,
        groupeCoursId: this.form.get('group')?.value,
      })
      .then((res) => {
          //Pass if success
          console.log('Success', res)
          location.reload();
          this.showAddLesson();
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
          this.loading = false;
      })
  }
}

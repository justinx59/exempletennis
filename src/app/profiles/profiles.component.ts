import { Component, OnInit } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import API from '../api';

registerLocaleData(localeFr);

//Calendar
import {
  CalendarView,
  CalendarEvent,
} from 'angular-calendar';
import * as moment from 'moment';

function toDateWithOutTimeZone(date: any) {
  let tempTime = date.split(":");
  let dt = new Date();
  dt.setHours(tempTime[0]);
  dt.setMinutes(tempTime[1]);
  dt.setSeconds(tempTime[2]);
  return dt;
}

interface Profile{
  id: string,
  name: string,
  surname: string,
  sexe: string,
  lessonType: string,
  birthDate: Date,
  blessed: boolean,
  level: string,
  ranking: string,
  comment: string,
}

interface Lesson{
  id: string,
}

interface Disponibility{
  id: string,
  start_hour: Date,
  end_hour: Date,
  day: string,
  period: boolean,
  first: boolean,
}

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: [ './profiles.component.css' ],
})

export class ProfilesComponent implements OnInit {
    profiles: Array<Profile> = []
    lessons: Array<Lesson> = []
    disponibilities: Array<Disponibility> = []
    indexSelected: number = 0;

    locale: string = "fr";
    view: CalendarView = CalendarView.Month;
    CalendarView = CalendarView;
    viewDate: Date = new Date();

    addProfileVisible: boolean = false;
    disponibilityVisible: boolean = false;
    modifyDisponibilities: boolean = false;
    modifyDataVisible: boolean = false;
    commentVisible: boolean = false;
    requestLessonVisible: boolean = false;

    events: CalendarEvent[] = [
      {
        start: moment().toDate(),
        end: moment().toDate(),
        title: 'A 1 day event',
        allDay: true,
      },
    ]

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        this.viewDate = date;
    }

    //Function launch when component created
    ngOnInit(): void {
        //Find User Lessons data !
        let session = sessionStorage.getItem('userData')
        let userData = session ? JSON.parse(session) : undefined;
        if(userData) this.findProfiles(userData.id); 
    }

    //Call API for profiles
    findProfiles(id: string): void{
      //Init new Array
      let newProfiles: Profile[] = []

      API.get(`/user/${id}`)
      .then((res) => {
        res.data.profils.forEach((element: any) => {
          let newProfile: Profile = {
            id: element.id,
            name: element.nom,
            surname: element.prenom,
            sexe: element.sexe,
            lessonType: element.typeCours,
            birthDate: new Date(element.dateNaissance),
            blessed: element.blesse,
            level: element.niveau,
            ranking: element.classement,
            comment: element.commentaire,
          }

          //Add new Profile
          newProfiles.push(newProfile)
        });
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
          //Edit component Profiles
          this.profiles = newProfiles

          this.findUserDisponibilites(this.profiles[this.indexSelected].id);
      })
    }

    //Call API for disponibilities
    findUserDisponibilites(id: string){
      //Init new Array
      let newDisponibilities: Disponibility[] = []

      API.get(`/profil/${this.profiles[this.indexSelected].id}`)
      .then((res) => {
        res.data.disponibilites.reverse().forEach((element: any) => {

          let first = newDisponibilities.find((elmt) => elmt.day===element.jour)===undefined

          //Create new Disponibility
          let newDisponibility:Disponibility = {
            id: element.id,
            start_hour: toDateWithOutTimeZone(element.heureDebut),
            end_hour: toDateWithOutTimeZone(element.heureFin),
            day: element.jour,
            period: element.periode==="Septembre-Mars"?true:false,
            first: first
          }

          //Add new Profile
          newDisponibilities.push(newDisponibility)
        });
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
      })

      //Edit component Disponibilities
      this.disponibilities = newDisponibilities
    }

    //Edit comment area
    editComment($event: any): void{
      this.profiles[this.indexSelected].comment = $event.target.value;
    }

    //Update comment
    updateComment = (): void => {
      API.put(`/profil/${this.profiles[this.indexSelected].id}`,{
        id: this.profiles[this.indexSelected].id,
        nom: this.profiles[this.indexSelected].name,
        prenom: this.profiles[this.indexSelected].surname,
        sexe: this.profiles[this.indexSelected].sexe,
        dateNaissance: this.profiles[this.indexSelected].birthDate,
        classement: this.profiles[this.indexSelected].ranking,
        niveau: this.profiles[this.indexSelected].level,
        typeCours: this.profiles[this.indexSelected].lessonType,
        commentaire: this.profiles[this.indexSelected].comment,
      })
      .then((res) => {
          this.showComment();
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
      })
    }

    //Edit disponibilities
    editDisponibility($event: any, day: string, start: boolean, first: boolean, firstPeriod: boolean): void{
      let disponibility = this.disponibilities.find((elmt) => elmt.day===day && elmt.first===first && elmt.period===firstPeriod)

      if(!disponibility || disponibility===undefined){
        let newDisponibility: Disponibility = {
          id:"",
          start_hour:start?toDateWithOutTimeZone($event.target.value+':00'):toDateWithOutTimeZone("00:00:00"),
          end_hour:!start?toDateWithOutTimeZone($event.target.value+':00'):toDateWithOutTimeZone("00:00:00"),
          day:day,
          period:firstPeriod,
          first:first
        }

        this.disponibilities.push(newDisponibility)

        return
      }
      
      if(start)
        disponibility.start_hour=toDateWithOutTimeZone($event.target.value+':00')
      else
        disponibility.end_hour=toDateWithOutTimeZone($event.target.value+':00')
    }

    //Affichage des disponibilité
    getDisponibility(day: string, first: boolean, firstPeriod: boolean): string{
      let dday = this.disponibilities.find((elmt) => elmt.day===day && elmt.first===first && elmt.period===firstPeriod)
      if(dday)
        return `${dday.start_hour.getHours().toString().padStart(2, '0')}h${dday.start_hour.getMinutes().toString().padStart(2, '0')}-${dday.end_hour.getHours().toString().padStart(2, '0')}h${dday.end_hour.getMinutes().toString().padStart(2, '0')}`
      
      return "Aucune"
    }

    //Affichage des disponibilité
    getModifyDisponibility(day: string, start: boolean, first: boolean, firstPeriod: boolean): string{
      let dday = this.disponibilities.find((elmt) => elmt.day===day && elmt.first===first && elmt.period===firstPeriod)
      if(dday && start)
        return `${dday.start_hour.getHours().toString().padStart(2, '0')}:${dday.start_hour.getMinutes().toString().padStart(2, '0')}`
      
      if(dday && !start)
        return `${dday.end_hour.getHours().toString().padStart(2, '0')}:${dday.end_hour.getMinutes().toString().padStart(2, '0')}`

      return "Aucune"
    }

    //Submit disponibilities modification
    sumbitDisponibility(): void{
      this.disponibilities.forEach((elmt) => {
        if(elmt.id===""){
          this.addDisponibility(elmt)
        }else{
          this.updateDisponibility(elmt.id, elmt)
        }
      })

      location.reload();
    }

    //Set Disponibility
    updateDisponibility(id: string, disponibility: Disponibility): void{
      API.put(`/disponibilite/${id}`,{
        id: id,
        heureDebut: disponibility.start_hour.toLocaleTimeString(),
        heureFin: disponibility.end_hour.toLocaleTimeString(),
        jour: disponibility.day,
        periode: disponibility.period?"Septembre-Mars":"Septembre-Juin",
      })
      .then((res) => {
          
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
      })
    }

    //Modification des disponibilité
    addDisponibility(disponibility: Disponibility): void{
      API.post(`/disponibilite`,{
        profilId: this.profiles[this.indexSelected].id,
        heureDebut: disponibility.start_hour.toLocaleTimeString(),
        heureFin: disponibility.end_hour.toLocaleTimeString(),
        jour: disponibility.day,
        periode: disponibility.period?"Septembre-Mars":"Septembre-Juin",
      })
      .then((res) => {
          console.log('Success', res.data)
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
      })
    }

    //Call API for lessons
    findUserLessons(id: string){
      //Init new Array
      let newlessons = []

      //Create new Disponibility
      let newLesson:Lesson = {
        id: "48481781738555",
      } 

      //Add new Disponibility
      newlessons.push(newLesson)

      //Edit component Disponibilities
      this.lessons = newlessons
    }

    //Change selected profile
    selectProfile(index: number): void{
      this.indexSelected=index;
      this.findUserDisponibilites(this.profiles[index].id);
    }

    //Show add profile menu
    showAddProfile = (): void => {
      this.addProfileVisible=!this.addProfileVisible
    }

    //Show add disponibility menu
    showAddDisponibility = (): void => {
      this.disponibilityVisible=!this.disponibilityVisible
    }

    //Show Modify disponibility menu
    showModifyDisponibility = (): void => {
      this.modifyDisponibilities=!this.modifyDisponibilities
    }

    //Show comment menu
    showComment = (): void => {
      this.commentVisible=!this.commentVisible
    }

    //Show add disponibility menu
    showModifyData = (): void => {
      this.modifyDataVisible=!this.modifyDataVisible
    }

    //Show add requestLessonVisible menu
    showRequestLesson = (): void => {
      this.requestLessonVisible=!this.requestLessonVisible
    }
}
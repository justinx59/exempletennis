import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css' ]
})

export class HomeComponent{
    userConnected: boolean = false;
    isResponsable: boolean = false;
    isTeacher: boolean = false;

    //Function launch when component created
    ngOnInit(): void {
        //Find if User is Connected to change access !
        let session = sessionStorage.getItem('userData')
        let userData = session ? JSON.parse(session) : undefined;
        this.userConnected = userData?userData.id:false; 
        this.isResponsable = userData?userData.responsable:false; 
        this.isTeacher = userData?userData.profils.find((elmt: any) => elmt.professeur===true)!==undefined:false;
  }
}
import { Component, Input, Output } from '@angular/core';
import API from '../api';

interface Profile {
  id: string,
  user_name: string,
  user_surname: string,
  teacher: boolean,
}

@Component({
  selector: 'app-add-teacher',
  templateUrl: './add-teacher.component.html',
  styleUrls: ['./add-teacher.component.css']
})

export class AddTeacherComponent {
  @Input() isVisible: boolean = false;
  @Input() showAddTeacher: () => void = () => {return };

  profiles: Array<Profile> = [];

  //Function launch when component created
  ngOnInit(): void {
      //Find User Profiles data !
      let session = sessionStorage.getItem('userData')
      let userData = session ? JSON.parse(session) : undefined;
      if(userData) this.findProfiles(userData.id); 
  }

  //Call API for profiles
  findProfiles(id: number): void{
    //Init new Array
    let newProfiles: Profile[] = []

    API.get('/profil')
    .then((res) => {
      res.data.filter((element: any) => !element.professeur).forEach((element: any) => {
        //Create new Profile
        let newLesson:Profile = {
          id: element.id,
          user_name: element.nom,
          user_surname: element.prenom,
          teacher: element.responsable,
        }

        //Add new Profile
        newProfiles.push(newLesson)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')

        //Edit component Profiles
        this.profiles = newProfiles
    })
  }

  //Set Teacher
  updateProfiles(id: string): void{
    API.put(`/profil/${id}`,{
      professeur: true
    })
    .then((res) => {
        location.reload();
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
    })
  }
}

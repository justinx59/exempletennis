import { Component } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import API from '../api';

registerLocaleData(localeFr);

//Calendar
import {
  CalendarView,
  CalendarEvent,
} from 'angular-calendar';

interface Profile{
  id: string,
  name: string,
  surname: string,
  sexe: string,
  lessonType: string,
  birthDate: Date,
  blessed: boolean,
  level: string,
  ranking: string,
  isTeacher: boolean,
}

interface Groupe{
  id: string,
  day: string,
  startHour: string,
  endHour: string,
  period: string,
  size: number,
}

interface Lesson{
  id: string,
  isDone: boolean,
  willBe: boolean,
  group: Groupe,
  date: Date,
  students: Array<Profile>
  teachers: Array<Profile>
}

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.css']
})

export class LessonsComponent {
  lessons: Array<Lesson> = []
  events: CalendarEvent[] = []

  addLessonVisible: boolean = false;

  locale: string = "fr";
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();

  //Function launch when component created
  ngOnInit(): void {
    this.getLessons()
  }

  //Get all lessons
  getLessons(): void{
    //Init new Array
    let newLessons: Lesson[] = []
    let newEvents: CalendarEvent[] = []

    API.get(`/cours`)
    .then((res) => {
      res.data.forEach((element: any) => {
        let newProfiles: Profile[] = []

        element.groupeCours.profils.forEach((elmt: any) => {
          let newProfile: Profile = {
            id: elmt.id,
            name: elmt.nom,
            surname: elmt.prenom,
            sexe: elmt.sexe,
            lessonType: elmt.typeCours,
            birthDate: new Date(elmt.dateNaissance),
            blessed: elmt.blesse,
            level: elmt.niveau,
            ranking: elmt.classement,
            isTeacher: elmt.professeur,
          }

          newProfiles.push(newProfile)
        });

        let newGroup: Groupe = {
          id: element.groupeCours.id,
          day: element.groupeCours.jour,
          startHour: element.groupeCours.heureDebut,
          endHour: element.groupeCours.heureFin,
          period: element.groupeCours.periode,
          size: element.groupeCours.tailleGroupe,
        }

        let newLesson: Lesson = {
          id: element.id,
          isDone: element.aEteDonne,
          willBe: element.vaEtreDonne,
          group: newGroup,
          date: element.date,
          students: newProfiles.filter((elmt) => !elmt.isTeacher),
          teachers: newProfiles.filter((elmt) => elmt.isTeacher),
        }

        let startDate = new Date(element.date)
        startDate.setHours(element.groupeCours.heureDebut.split(':')[0])
        startDate.setMinutes(element.groupeCours.heureDebut.split(':')[1])

        let endDate = new Date(element.date)
        endDate.setHours(element.groupeCours.heureFin.split(':')[0])
        endDate.setMinutes(element.groupeCours.heureFin.split(':')[1])

        let newEvent: CalendarEvent = {
          start: startDate,
          end: endDate,
          title: 'A 1 day event',
          allDay: true,
        }

        //Add new Lesson and Calendar Event
        newLessons.push(newLesson)
        newEvents.push(newEvent)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
        //Edit component Lessons
        this.lessons = newLessons
        this.events = newEvents
    })

    API.get(`/coursalacarte`)
    .then((res) => {
      res.data.forEach((element: any) => {
        let newProfiles: Profile[] = []

        element.groupeCours.profils.forEach((elmt: any) => {
          let newProfile: Profile = {
            id: elmt.id,
            name: elmt.nom,
            surname: elmt.prenom,
            sexe: elmt.sexe,
            lessonType: elmt.typeCours,
            birthDate: new Date(elmt.dateNaissance),
            blessed: elmt.blesse,
            level: elmt.niveau,
            ranking: elmt.classement,
            isTeacher: elmt.professeur,
          }

          newProfiles.push(newProfile)
        });

        let newGroup: Groupe = {
          id: element.groupeCours.id,
          day: element.groupeCours.jour,
          startHour: element.groupeCours.heureDebut,
          endHour: element.groupeCours.heureFin,
          period: element.groupeCours.periode,
          size: element.groupeCours.tailleGroupe,
        }

        let newLesson: Lesson = {
          id: element.id,
          isDone: element.aEteDonne,
          willBe: element.vaEtreDonne,
          group: newGroup,
          date: element.date,
          students: newProfiles.filter((elmt) => !elmt.isTeacher),
          teachers: newProfiles.filter((elmt) => elmt.isTeacher),
        }

        let newEvent: CalendarEvent = {
          start: new Date(),
          end: new Date(),
          title: 'A 1 day event',
          allDay: true,
        }

        //Add new Lesson and Calendar Event
        newLessons.push(newLesson)
        newEvents.push(newEvent)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
        //Edit component Lessons
        this.lessons = newLessons
        this.events = newEvents
    })
  }

  //Change day clicked
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
      this.viewDate = date;
  }

  //Show add Lesson menu
  showAddLesson = (): void => {
    this.addLessonVisible=!this.addLessonVisible
  }

  getActualLesson() : Lesson[]{
    return this.lessons.filter((elmt) => new Date(elmt.date).toLocaleDateString()===this.viewDate.toLocaleDateString())
  }
}

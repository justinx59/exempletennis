import { Component } from '@angular/core';
import API from '../api';

interface Teacher{
  id: string,
  name: string,
  surname: string,
  sexe: string,
  lessonType: string,
  birthDate: Date,
  blessed: boolean,
  level: string,
  ranking: string,
  groups: Groupe[]
}

interface Groupe{
  id: string,
  day: string,
  startHour: string,
  endHour: string,
  period: string,
  size: number,
}

interface Lesson{
  id: string,
}

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})

export class TeachersComponent {
  teachers: Array<Teacher> = []
  lessons: Array<Lesson> = []
  indexSelected: number = 0;

  addTeacherVisible: boolean = false;
  addToGroupVisible: boolean = false;

  //Function launch when component created
  ngOnInit(): void {
      //Find User Lessons data !
      let session = sessionStorage.getItem('userData')
      let userData = session ? JSON.parse(session) : undefined;
      if(userData) this.findProfiles(userData.id); 
  }

  //Call API for profiles
  findProfiles(id: string): void{
    //Init new Array
    let newTeachers: Teacher[] = []

    let session = sessionStorage.getItem('userData');
    let userData = session ? JSON.parse(session) : undefined;

    if(userData){
      API.get(`/profil`)
      .then((res) => {
        res.data.filter((element: any) => element.professeur).forEach((element: any) => {
          let newGroups: Array<Groupe> = [];

          element.groupesCours.forEach((elmt: any) => {
            let newGroup: Groupe = {
              id: elmt.id,
              day: elmt.jour,
              startHour: elmt.heureDebut,
              endHour: elmt.heureFin,
              period: elmt.periode,
              size: elmt.tailleGroupe,
            }
    
            //Add new Group
            newGroups.push(newGroup)
          });

          let newTeacher: Teacher = {
            id: element.id,
            name: element.nom,
            surname: element.prenom,
            sexe: element.sexe,
            lessonType: element.typeCours,
            birthDate: new Date(element.dateNaissance),
            blessed: element.blesse,
            level: element.niveau,
            ranking: element.classement,
            groups: newGroups,
          }

          //Add new Profile
          newTeachers.push(newTeacher)
        });
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
          //Edit component Profiles
          this.teachers = newTeachers
      })
    };
  }

  //Change selected profile
  selectProfile(index: number): void{
    this.indexSelected=index;
  }

  //Show add teacher menu
  showAddTeacher = (): void => {
    this.addTeacherVisible=!this.addTeacherVisible;
  }

  //Show add group menu
  showAddToGroup = (): void => {
    this.addToGroupVisible=!this.addToGroupVisible;
  }
}

import { Component } from '@angular/core';
import API from '../api';

interface Profile {
  id: string,
  user_name: string,
  user_surname: string,
  teacher: boolean,
  ranking: string,
}

@Component({
  selector: 'app-last-profiles',
  templateUrl: './last-profiles.component.html',
  styleUrls: ['./last-profiles.component.css']
})
export class LastProfilesComponent {
  profiles: Array<Profile> = [];

  //Function launch when component created
  ngOnInit(): void {
      //Find User Last Profiles data !
      let session = sessionStorage.getItem('userData')
      let userData = session ? JSON.parse(session) : undefined;
      if(userData) this.findProfiles(userData.id); 
  }

  //Call API for Last Profile
  findProfiles(id: number): void{
    //Init new Array
    let newProfiles: Profile[] = []

    API.get('/profil')
    .then((res) => {
      res.data.forEach((element: any) => {
        //Create new Last Profile
        let newProfile:Profile = {
          id: element.id,
          user_name: element.nom,
          user_surname: element.prenom,
          teacher: element.professeur,
          ranking: element.classement,
        }

        //Add new Last Profile
        newProfiles.push(newProfile)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')

        //Edit component Last Profiles
        this.profiles = newProfiles
    })
  }
}

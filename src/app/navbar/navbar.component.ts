import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [ './navbar.component.css' ]
})

export class NavbarComponent implements OnInit {
    userConnected: boolean = false;
    isResponsable: boolean = false;
    isTeacher: boolean = false;
    username: string = "Guest";

    //Function launch when component created
    ngOnInit(): void {
        //Find User data !
        let session = sessionStorage.getItem('userData')
        let userData = session ? JSON.parse(session) : undefined;
        this.userConnected = userData?userData.id:false;  
        this.isResponsable = userData?userData.responsable:false; 
        this.username = userData?userData.nom+" "+userData.prenom:"Guest";
        this.isTeacher = userData?userData.profils.find((elmt: any) => elmt.professeur===true)!==undefined:false;
    }

    //Disconnect User
    logout(): void {
      sessionStorage.removeItem('userData');
      sessionStorage.removeItem('token');
      location.reload();
    }
}
import { Component } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import API from '../api';

registerLocaleData(localeFr);

//Calendar
import {
  CalendarView,
  CalendarEvent,
} from 'angular-calendar';

function toDateWithOutTimeZone(date: any) {
  let tempTime = date.split(":");
  let dt = new Date();
  dt.setHours(tempTime[0]);
  dt.setMinutes(tempTime[1]);
  dt.setSeconds(tempTime[2]);
  return dt;
}

interface Request {
  id: string,
  profilId: string,
  type: string,
  groupSize: number,
  cycle: string,
  valide: boolean,
  lessonNumber: number,
  nom: string,
  prenom: string,
  disponibilities: Array<Disponibility>,
}

interface Lesson{
  id: string,
  start: Date,
  end: Date,
}

interface Groupe{
  id: string,
  day: string,
  startHour: string,
  endHour: string,
  period: string,
  size: number,
  profils: Array<string>
}

interface Disponibility{
  id: string,
  start_hour: Date,
  end_hour: Date,
  day: string,
  period: boolean,
  first: boolean,
}

@Component({
  selector: 'app-lesson-requests',
  templateUrl: './lesson-requests.component.html',
  styleUrls: ['./lesson-requests.component.css']
})

export class LessonRequestsComponent {
  lessons: Array<Lesson> = []
  requests: Array<Request> = [];
  groups: Array<Groupe> = [];
  idSelected: string = "";
  indexSelected: number = 0;
  groupIndexSelected: number = 0;
  events: CalendarEvent[] = []
  addGroupVisible: boolean = false;

  locale: string = "fr";
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();

  //Function launch when component created
  ngOnInit(): void {
    this.findRequests(); 
    this.getLessons();
    this.getGroups();
  }

  getGroups(): void{
    //Init new Array
    let newGroups: Groupe[] = []

    API.get(`/groupecours`)
    .then((res) => {
      res.data.forEach((element: any) => {
        let newGroup: Groupe = {
          id: element.id,
          day: element.jour,
          startHour: element.heureDebut,
          endHour: element.heureFin,
          period: element.periode,
          size: element.tailleGroupe,
          profils: element.profils.map((profil : any) => {return profil.id})
        }

        //Add new Group
        newGroups.push(newGroup)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
        //Edit component Groups
        this.groups = newGroups
    })
  }

  //Post to add to group
  addToGroup(): void{
    //API Call to create profile
    API.post('/profilgroupecours',{
      groupeCoursId: this.groups[this.groupIndexSelected].id,
      profilIds: [this.requests[this.indexSelected].profilId],
    })
    .then((res) => {
        //Pass if success
        console.log('Success', res)
        location.reload();
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
    })
  }

  //Get all lessons
  getLessons(): void{
    //Init new Array
    let newLessons: Lesson[] = []
    let newEvents: CalendarEvent[] = []

    API.get(`/cours`)
    .then((res) => {
      res.data.forEach((element: any) => {
        let startDate = new Date(element.date)
        startDate.setHours(element.groupeCours.heureDebut.split(':')[0])
        startDate.setMinutes(element.groupeCours.heureDebut.split(':')[1])

        let endDate = new Date(element.date)
        endDate.setHours(element.groupeCours.heureFin.split(':')[0])
        endDate.setMinutes(element.groupeCours.heureFin.split(':')[1])

        let newEvent: CalendarEvent = {
          start: startDate,
          end: endDate,
          title: 'A 1 day event',
          allDay: true,
        }

        let newLesson: Lesson = {
          id: element.id,
          start: startDate,
          end: endDate,
        }

        //Add new Lesson and Calendar Event
        newLessons.push(newLesson)
        newEvents.push(newEvent)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
        //Edit component Lessons
        this.lessons = newLessons
        this.events = newEvents
    })

    API.get(`/coursalacarte`)
    .then((res) => {
      res.data.forEach((element: any) => {
        let startDate = new Date(element.date)
        startDate.setHours(element.groupeCours.heureDebut.split(':')[0])
        startDate.setMinutes(element.groupeCours.heureDebut.split(':')[1])

        let endDate = new Date(element.date)
        endDate.setHours(element.groupeCours.heureFin.split(':')[0])
        endDate.setMinutes(element.groupeCours.heureFin.split(':')[1])

        let newEvent: CalendarEvent = {
          start: startDate,
          end: endDate,
          title: 'A 1 day event',
          allDay: true,
        }

        let newLesson: Lesson = {
          id: element.id,
          start: startDate,
          end: endDate,
        }

        //Add new Lesson and Calendar Event
        newLessons.push(newLesson)
        newEvents.push(newEvent)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
        //Edit component Lessons
        this.lessons = newLessons
        this.events = newEvents
    })
  }

  //Call API for Requests
  findRequests(): void{
    //Init new Array
    let newRequests: Request[] = []

    API.get('/demandecours')
    .then((res) => {
      res.data.filter((element: any) => !element.valide).forEach((element: any) => {
        let newDisponibilities: Disponibility[] = []
        
        element.profil.disponibilites.filter((elmt: any) => element.typeCycle.includes(elmt.periode)).forEach((element: any) => {
          let first = newDisponibilities.find((elmt) => elmt.day===element.jour)===undefined

          //Create new Disponibility
          let newDisponibility:Disponibility = {
            id: element.id,
            start_hour: toDateWithOutTimeZone(element.heureDebut),
            end_hour: toDateWithOutTimeZone(element.heureFin),
            day: element.jour,
            period: element.periode==="Septembre-Mars"?true:false,
            first: first
          }

          //Add new Profile
          newDisponibilities.push(newDisponibility)
        });

        //Create new Request
        let newRequest:Request = {
          id: element.id,
          profilId: element.profilId,
          type: element.type,
          groupSize: element.tailleGroupe,
          cycle: element.typeCycle,
          valide: element.valide,
          lessonNumber: element.nbCoursParSemaine,
          nom: element.profil.nom,
          prenom: element.profil.prenom,
          disponibilities: newDisponibilities,
        }

        this.idSelected = element.id;

        //Add new Request
        newRequests.push(newRequest)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')

        //Edit component Requests
        this.requests = newRequests
    })
  }

  validateRequest(): void{
    API.put(`/demandecours/${this.requests[this.indexSelected].id}`,{
      valide: true,
      type: this.requests[this.indexSelected].type,
      typecycle: this.requests[this.indexSelected].cycle,
    })
    .then((res) => {
      
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
    })
  }

  getCycleMars(): Request[]{
    return this.requests.filter((elmt) => elmt.cycle.includes("Septembre-Mars"))
  }

  getCycleJuin(): Request[]{
    return this.requests.filter((elmt) => elmt.cycle.includes("Septembre-Juin"))
  }

  getCycleGroup(): Groupe[]{
    return this.groups.filter((elmt) => elmt.period.includes(this.requests[this.indexSelected].cycle))
  }

  canAdd(): boolean{
    return this.groups[this.groupIndexSelected].profils.find((elmt) => elmt===this.requests[this.indexSelected].profilId)===undefined;
  }

  //Change selected request
  selectRequest(id: string): void{
    this.idSelected = id;
    this.indexSelected=this.requests.findIndex((elmt) => elmt.id === id);
  }

  //Change selected group
  selectGroup(index: number): void{
    this.groupIndexSelected=index;
  }

  //Change day clicked
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    this.viewDate = date;
  }

  //Show add group menu
  showAddGroup = (): void => {
    this.addGroupVisible=!this.addGroupVisible
  }

  getActualLesson() : Lesson[]{
    return this.lessons.filter((elmt) => new Date(elmt.start).toLocaleDateString()===this.viewDate.toLocaleDateString())
  }
}

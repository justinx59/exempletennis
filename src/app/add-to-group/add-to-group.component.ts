import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

interface Groupe{
  id: string,
  day: string,
  startHour: string,
  endHour: string,
  period: string,
  size: number,
}

@Component({
  selector: 'app-add-to-group',
  templateUrl: './add-to-group.component.html',
  styleUrls: ['./add-to-group.component.css']
})

export class AddToGroupComponent {
  @Input() isVisible: boolean = false
  @Input() showAddToGroup: () => void = () => {return };
  @Input() profilId: string = ""
  groups: Array<Groupe> = [];

  form: FormGroup = new FormGroup({
    group: new FormControl("",[
        Validators.required,
    ]),
  });
  loading = false;
  submitted = false;

  get group() { return this.form.get('group'); }

  //Function launch when component created
  ngOnInit(): void {
    this.getGroups();
  }

  getGroups(): void{
    //Init new Array
    let newGroups: Groupe[] = []

    API.get(`/groupecours`)
    .then((res) => {
      res.data.forEach((element: any) => {
        let newGroup: Groupe = {
          id: element.id,
          day: element.jour,
          startHour: element.heureDebut,
          endHour: element.heureFin,
          period: element.periode,
          size: element.tailleGroupe,
        }

        //Add new Group
        newGroups.push(newGroup)
      });
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
        //Edit component Groups
        this.groups = newGroups
    })
  }

  onSubmit() {
    this.submitted = true;

    //Stop submit if invalid
    if (this.form.invalid) {
        return;
    }

    this.loading = true;

    //API Call to create profile
    API.post('/profilgroupecours',{
      groupeCoursId: this.form.get('group')?.value,
      profilIds: [this.profilId],
    })
    .then((res) => {
        //Pass if success
        console.log('Success', res)
        location.reload();
    })
    .catch((err) => {
        //Pass if errors
        console.log('Error', err)
    })
    .finally(() => {
        //Pass all time
        console.log('End')
    })
  }
}

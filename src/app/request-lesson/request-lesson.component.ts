import { Component, Input } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import API from '../api';

interface Profile{
  id: string,
  name: string,
  surname: string,
  sexe: string,
  lessonType: string,
  birthDate: Date,
  blessed: boolean,
  level: string,
  ranking: string,
  comment: string,
}

@Component({
  selector: 'app-request-lesson',
  templateUrl: './request-lesson.component.html',
  styleUrls: ['./request-lesson.component.css']
})

export class RequestLessonComponent {
  @Input() isVisible: boolean = false;
  @Input() showRequestLesson: () => void = () => {return };
  @Input() index: number = 0;
  @Input() profiles: Array<Profile> = []
  userId: string = "";

  form: FormGroup = new FormGroup({
    groupSize: new FormControl("",[
        Validators.required,
    ]),
    lessonNumber: new FormControl("",[
        Validators.required,
    ]),
    cycle: new FormControl("",[
        Validators.required,
    ]),
    lessontype: new FormControl("",[
      Validators.required,
    ])
  });
  loading = false;
  submitted = false;

  get groupSize() { return this.form.get('groupSize'); }
  get lessonNumber() { return this.form.get('lessonNumber'); }
  get cycle() { return this.form.get('cycle'); }
  get lessontype() { return this.form.get('lessontype'); }

  //Function launch when component created
  ngOnInit(): void {
    //Find User Lessons data !
    let session = sessionStorage.getItem('userData')
    let userData = session ? JSON.parse(session) : undefined;
    this.userId = userData.id;
  }

  onSubmit() {
      this.submitted = true;

      //Stop submit if invalid
      if (this.form.invalid) {
          return;
      }

      this.loading = true;

      //API Call to create profile
      API.post('/demandecours',{
        userId: this.userId,
        profilId: this.profiles[this.index].id,
        type: this.form.get('lessontype')?.value,
        tailleGroupe: this.form.get('groupSize')?.value,
        typeCycle: this.form.get('cycle')?.value,
        nbCoursParSemaine: this.form.get('lessonNumber')?.value,
      })
      .then((res) => {
          //Pass if success
          console.log('Success', res)
          location.reload();
          this.showRequestLesson();
      })
      .catch((err) => {
          //Pass if errors
          console.log('Error', err)
      })
      .finally(() => {
          //Pass all time
          console.log('End')
          this.loading = false;
      })
  }
}
